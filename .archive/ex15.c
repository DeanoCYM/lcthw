#include <stdio.h>

void print_by_indexing(int ages[], char* names[], int count)
{
    int i = 0;
    for ( i = 0; i < count; i++ ) {
	printf("%s is %d years old.\n", names[i], ages[i]);
    }

    i = 0;
    while ( i < count ) {
	printf("%s is %d years old.\n", names[i], ages[i]);
	i++;
    }

    printf("---\n");
}

void print_by_pointer_arithmatic(int *cur_ages, char **cur_names, int count)
{
    int i = 0;
    for ( i = 0; i < count; i++ ) {
	printf("%s is %d years old.\n", *(cur_names + i), *(cur_ages + i));
    }

    i = 0 ;
    while ( i < count ) {
	printf("%s is %d years old.\n", *(cur_names + i), *(cur_ages + i));
	i++;
    }

    printf("---\n");
}

void print_by_ptr_as_array(int *cur_ages, char **cur_names, int count)
{
    int i = 0;
    for ( i = 0; i < count; i++ ) {
	printf("%s is %d years old.\n", cur_names[i], cur_ages[i]);
    }
    
    i = 0;
    while ( i < count ) {
	printf("%s is %d years old.\n", cur_names[i], cur_ages[i]);
	i++;
    }

    printf("---\n");
}

void print_complex(int ages[], char *names[],
		   int *cur_ages, char **cur_names, int count)
{
    for (cur_names = names, cur_ages = ages;
	 (cur_ages - ages) < count;
	 cur_names++, cur_ages++) {
	printf("%s is %d years old.\n", *cur_names, *cur_ages);
    }

    cur_names = names;
    cur_ages = ages;
    while ( (cur_ages - ages) < count ) {
	printf("%s is %d years old.\n", *cur_names, *cur_ages);
	cur_names++;
	cur_ages++;
    }
}

int main(int argc, char *argv[])
{
    
    int ages[] = { 12, 13, 14, 15, 16 };
    char *names[] = {
	"Rhys", "Ellis", "Thomas",
	"Ian", "Lorraine"
    };

    int count = sizeof ( ages ) / sizeof ( int );
    int *cur_ages = ages;
    char **cur_names = names;
    
    print_by_indexing(ages, names, count);
    print_by_pointer_arithmatic(cur_ages, cur_names, count);
    print_by_ptr_as_array(cur_ages, cur_names, count);
    print_complex(ages, names, cur_ages, cur_names, count);

    int i = 0;
    char **cur_argv = argv;

    for ( i = 0; i < argc; i++ ) {
	printf("arg %d: %s\n", i, cur_argv[i]);
    }

    return 0;
}
