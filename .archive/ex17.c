#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

struct Address {
    int id;
    int set;
    char *name;
    char *email;
    char *job;
};

struct Database {
    size_t max_rows;
    size_t max_data;
    struct Address *rows;
};

struct Connection {
    FILE *file;
    struct Database *db;
};

void Database_close(struct Connection *conn);

void die(const char *message, struct Connection *conn)
{
    if (errno) {
	perror(message);
    } else {
	printf("ERROR: %s\n", message);
    }

    Database_close(conn);
    exit(1);
}

void id_check(struct Connection *conn, int id)
{
    if (id >= conn->db->max_rows)
	die("id cannot be greater than max rows", conn);
}

void Address_print(struct Address *addr)
{
    printf("%d %s %s %s\n", addr->id, addr->name, addr->job, addr->email);
}

void Database_load(struct Connection *conn)
{
    int rc = 0;
    int i = 0;
    
    for (i = 0; i < conn->db->max_rows; i++) {
	
	rc = fread(&(conn->db->rows[i].id), sizeof(int), 1, conn->file);
	if (rc != 1) die("Failed to load database (id).", conn);

	rc = fread(&(conn->db->rows[i].set), sizeof(int), 1, conn->file);
	if (rc != 1) die("Failed to load database (set).", conn);

	rc = fread(conn->db->rows[i].name, sizeof(char),
		   conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to load database (name)", conn);

	rc = fread(conn->db->rows[i].email, sizeof(char),
		   conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to load database (email)", conn);

	rc = fread(conn->db->rows[i].job, sizeof(char),
		   conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to load database (job)", conn);
    }
}

struct Connection *
Database_open(const char *filename, char mode, size_t max_rows,
	      size_t max_data)
{
    int rc = 0;
    int i = 0;

    struct Connection *conn = malloc(sizeof(struct Connection));
    if (!conn) die("Memory error", conn);

    conn->db = malloc(sizeof(struct Database));
    if (!conn->db) die("Memory error", conn);
    conn->db->rows = NULL;

    if (mode == 'c') {
	conn->file = fopen(filename, "w");
	if (!conn->file) die("Failed to open the file", conn);
	conn->db->max_rows = max_rows;
	conn->db->max_data = max_data;

    } else {

	conn->file = fopen(filename, "r+");
	if (!conn->file) die("Failed to open the file", conn);

	rc = fread(&(conn->db->max_rows), sizeof (int), 1, conn->file);
	if (rc != 1) die("Failed to read database size", conn);
	rc = fread(&(conn->db->max_data), sizeof (int), 1, conn->file);
	if (rc != 1) die("Failed to read database size", conn);
    }

    conn->db->rows = calloc(conn->db->max_rows, sizeof(struct Address));
    if (!conn->db->rows) die("Memory error", conn);

    for (i = 0; i < conn->db->max_rows; i++) {

	conn->db->rows[i].name = calloc(conn->db->max_data, sizeof (char));
	if (!conn->db->rows[i].name) die("Memory error", conn);

	conn->db->rows[i].email = calloc(conn->db->max_data, sizeof (char));
	if (!conn->db->rows[i].email) die("Memory error", conn);

	conn->db->rows[i].job = calloc(conn->db->max_data, sizeof (char));
	if (!conn->db->rows[i].job) die("Memory error", conn);
	
    }

    if (mode != 'c') {

	Database_load(conn);
    }

    return conn;
}

void Database_close(struct Connection *conn)
{
    int i = 0;

    if (conn) {
	if (conn->file) {
	    fclose(conn->file);
	}
	if (conn->db) {
	    if (conn->db->rows) {
		for (i = 0; i < conn->db->max_rows; i++) {
		    free(conn->db->rows[i].name);
		    free(conn->db->rows[i].email);
		    free(conn->db->rows[i].job);
		}
		free(conn->db->rows);
	    }
	    free(conn->db);
	}
	free(conn);
    }
}

void Database_write(struct Connection *conn)
{
    int rc = 0;
    int i = 0;
    rewind(conn->file);

    rc = fwrite(&(conn->db->max_rows), sizeof (int), 1, conn->file);
    if (rc != 1)
	die("Failed to write database (max_rows).\n", conn);

    rc = fwrite(&(conn->db->max_data), sizeof (int), 1, conn->file);
    if (rc != 1)
	die("Failed to write database (max_data).\n", conn);

    for (i = 0; i < conn->db->max_rows; i++) {
	rc = fwrite(&(conn->db->rows[i].id), sizeof(int), 1, conn->file);
	if (rc != 1) die("Failed to write database (id).\n", conn);

	rc = fwrite(&(conn->db->rows[i].set), sizeof(int), 1, conn->file);
	if (rc != 1) die("Failed to write database (set).\n", conn);

	rc = fwrite(conn->db->rows[i].name, sizeof(char),
		    conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to write database (name).\n", conn);

	rc = fwrite(conn->db->rows[i].email, sizeof(char),
		    conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to write database (email).\n", conn);

	rc = fwrite(conn->db->rows[i].job, sizeof(char),
		    conn->db->max_data, conn->file);
	if (rc != conn->db->max_data)
	    die("Failed to write database (job).\n", conn);
    }

    rc = fflush(conn->file);
    if (rc == -1)
	die("Cannot flush database.\n", conn);
}

void Database_create(struct Connection *conn)
{
    int i = 0;
    char *res = NULL;

    for (i = 0; i < conn->db->max_rows; i++) {
	conn->db->rows[i].id = i;
	conn->db->rows[i].set = 0;

	res = strncpy(conn->db->rows[i].name, "", conn->db->max_data);
	if (!res) die("Cannot prepare string memory", conn);
	conn->db->rows[i].name[conn->db->max_data - 1] = '\0';

	strncpy(conn->db->rows[i].email, "", conn->db->max_data);
	if (!res) die("Cannot prepare string memory", conn);
	conn->db->rows[i].email[conn->db->max_data - 1] = '\0';

	strncpy(conn->db->rows[i].job, "", conn->db->max_data);
	if (!res) die("Cannot prepare string memory", conn);
	conn->db->rows[i].job[conn->db->max_data - 1] = '\0';
    }
}

void Database_set(struct Connection *conn, int id, const char *name,
		  const char *email, const char *job)
{
    char *res = NULL;

    id_check(conn, id);

    struct Address *addr = &conn->db->rows[id];
    if (addr->set)
	die("Already set, delete it first", conn);

    addr->set = 1;

    res = strncpy(addr->name, name, conn->db->max_data);
    if (!res) die("Name copy failed", conn);
    addr->name[conn->db->max_data - 1] = '\0';

    res = strncpy(addr->email, email, conn->db->max_data);
    if (!res) die("Email copy failed", conn);
    addr->email[conn->db->max_data - 1] = '\0';

    res = strncpy(addr->job, job, conn->db->max_data);
    if (!res) die("Job copy failed", conn);
    addr->job[conn->db->max_data - 1] = '\0';
}

void Database_get(struct Connection *conn, int id)
{
    id_check(conn, id);

    struct Address *addr = &conn->db->rows[id];

    if (addr->set) {
	Address_print(addr);
    } else {
	die("ID is not set", conn);
    }
}

void Database_delete(struct Connection *conn, int id)
{
    char *res = NULL;

    id_check(conn, id);

    conn->db->rows[id].set = 0;

    res = strncpy(conn->db->rows[id].name, "", conn->db->max_data);
    if (!res) die("Name delete failed", conn);

    res = strncpy(conn->db->rows[id].email, "", conn->db->max_data);
    if (!res) die("Email delete failed", conn);

    res = strncpy(conn->db->rows[id].job, "", conn->db->max_data);
    if (!res) die("Job delete failed", conn);
}

void Database_list(struct Connection *conn)
{
    int i = 0;
    struct Database *db = conn->db;

    for (i = 0; i < conn->db->max_rows; i++) {
	struct Address *cur = &db->rows[i];

	if (cur->set) {
	    Address_print(cur);
	}
    }
}

int Database_find(struct Connection *conn, const char *search_str,
		  const char *search_field)
{
    int i = 0;
    int id = -1;
    struct Address *cur_addr = NULL;
    char *cur_field = NULL;
    size_t length = strlen(search_str);

    if (length >= conn->db->max_data) die("Search term too long", conn);

    for (i = 0; i < conn->db->max_data && id == -1; i++) {

	cur_addr = &(conn->db->rows[i]);

	if (!strcmp(search_field, "name")) {
	    cur_field = cur_addr->name;
	} else if (!strcmp(search_field, "job")) {
	    cur_field = cur_addr->job;
	} else {
	    die("No matching records", conn);
	}	    

	if (!strcmp(search_str, cur_field)) id = i;
    }

    return id;
}

int main(int argc, char *argv[])
{
    if (argc < 3)
	die("USAGE: ex17 <dbfile> <action> [action params]", NULL);

    char *filename = argv[1];
    char action = argv[2][0];
    int id = 0;
    size_t max_rows = 0;
    size_t max_data = 0;
    
    if (argc > 3 && action != 'c') {
	id = atoi(argv[3]);
    }
    if (action == 'c') {
	if (argc != 5) die("Need rows and data", NULL);
	max_rows = atoi(argv[3]);
	max_data = atoi(argv[4]);
    }

    struct Connection *conn =
	Database_open(filename, action, max_rows, max_data);

    switch (action) {
    case 'c':
	Database_create(conn);
	Database_write(conn);
	break;

    case 'g':
	if (argc != 4) die("Need an id to get", conn);
	    
	Database_get(conn, id);
	break;

    case 's':
	if (argc != 7)
	    die("Need id, name, email, job to set", conn);

	Database_set(conn, id, argv[4], argv[5], argv[6]);
	Database_write(conn);
	break;

    case 'd':
	if (argc != 4)
	    die("Need id to delete", conn);

	Database_delete(conn, id);
	Database_write(conn);
	break;

    case 'l':
	Database_list(conn);
	break;

    case 'f':
	if (argc != 5) die("Need a single search string and field", conn);
	id = Database_find(conn, argv[3], argv[4]);
	Database_get(conn, id);
	break;

    default:
	die("Invalid action: c=create, g=get, s=set, d=del, l=list f=find"
	    , conn);
    }

    Database_close(conn);

    return 0;
}

