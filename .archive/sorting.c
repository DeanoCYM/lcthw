#include <stdio.h>
#include <stdlib.h>

void numbers_print(int *numbers, int count)
{
    int i = 0;
    printf("[ ");
    for (i = 0; i < count; i++) {
	printf("%d ", numbers[i]);
    }
    printf("]\n");
}

int main (int argc, char *argv[])
{
    int count = argc - 1;
    char **arg = argv + 1;
    int i = 0;
    int j = 0;
    int tmp = 0;

    int *numbers = malloc(count * sizeof (int));

    for (i = 0; i < count; i++) {
	numbers[i] = atoi(arg[i]);
    }

    numbers_print(numbers, count);
    
    for (i = 0; i < (count - 1); i++) {	               
	if (numbers[i] > numbers[i+1]) {	       
	    for (j = 0; j < (count - 1); j++) {	       
		if (numbers[j] > numbers[i+1]) {       
		    tmp = numbers[j];
		    numbers[j] = numbers[i+1];
		    numbers[i+1] = tmp;
		}
	    }
	}
    }

    numbers_print(numbers, count);

    free(numbers);

    return 0;
}
