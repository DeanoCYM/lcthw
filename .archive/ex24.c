#include <stdio.h>
#include <stdlib.h>
#include "dbg.h"

#define MAX_DATA 100

typedef enum EyeColor
    {
     BLUE_EYES, GREEN_EYES, BROWN_EYES,
     BLACK_EYES, OTHER_EYES
    } EyeColor;
    
const char *EYE_COLOR_NAMES[] =
    {
     "Blue", "Green", "Brown", "Black", "Other"
    };

typedef struct Person {
    int age;
    char first_name[MAX_DATA];
    char last_name[MAX_DATA];
    EyeColor eyes;
    float income;
} Person;

int main(int argc, char *argv[])
{
    Person you = { .age = 0 };
    int i = 0;
    char *in = NULL;
    char tmp[MAX_DATA] = { 0 };

    printf("What's your first name? ");
    in = fgets(you.first_name, MAX_DATA - 1, stdin);
    check(in != NULL, "Failed to read first name.");

    printf("What's your Last Name? ");
    in = fgets(you.last_name, MAX_DATA - 1, stdin);
    check(in != NULL, "Failed to read last name.");
    
    printf("How old are you? ");
    in = fgets(tmp, MAX_DATA - 1, stdin);
    check(in != NULL, "Failed to read eye color");
    you.eyes = atoi(tmp);

    printf("What color are your eyes?\n");
    for (i = 0; i <= OTHER_EYES; i++) {
	printf("%d) %s\n", i + 1, EYE_COLOR_NAMES[i]);
    }

    printf("> ");
    in = fgets(tmp, MAX_DATA - 1, stdin);
    check(in != NULL, "Failed to read eye colour.");
    you.eyes = atoi(tmp) - 1;
    check(you.eyes <= OTHER_EYES && you.eyes >= 0,  "Failed to read eye colour.");


    printf("How much do you make an hour? ");
    in = fgets(tmp, MAX_DATA - 1, stdin);
    check(in != NULL, "Failed to read income.");
    you.income = strtof(tmp, NULL);

    printf("----------RESULTS----------\n");

    printf("First name: %s", you.first_name);
    printf("Last name: %s", you.last_name);
    printf("Age: %d\n", you.age);
    printf("Eye color: %s\n", EYE_COLOR_NAMES[you.eyes]);
    printf("Income: £%.2f\n", you.income);

    return 0;
 error:

    return -1;
}

