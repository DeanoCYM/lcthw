#include <stdio.h>
#include <limits.h>
#include "svs.h"
#include "../ert_log/ert_log.h"

#define SIZE 4

int
main (int argc, char **argv)
{

    char *vector[SIZE] = {"This", "is", "a", "vector"};
    const char *str1 = "Hello";
    const char *str2 = NULL;
    const char *str3 = "I'm going now!";

    struct Svs *Svs = svs_batch_create(vector, SIZE, PATH_MAX);
    svs_batch_print(&Svs);
    log_info("The length was %d and the maximum size was %zu",
	     Svs->count, Svs->path_max);
    
    svs_line_append(&Svs, str1);
    svs_line_append(&Svs, str2);
    svs_line_append(&Svs, str3);
    svs_batch_print(&Svs);
    log_info("The length was %d and the maximum size was %zu",
	     Svs->count, Svs->path_max);

    int i = 1;
    svs_line_delete(&Svs, &i);
    svs_batch_print(&Svs);
    log_info("The length was %d and the maximum size was %zu",
	     Svs->count, Svs->path_max);
    
    svs_batch_delete(&Svs);

    return 0;
}
