/* SVS.H (string vector structure)
 * --------------------------------------------------
 *
 * Functions for manipulating a structure containing an array of
 * pointers to strings. 
 *
 * (c) Ellis Rhys Thomas <e.rhys.thomas@gmail.com>
 *
 * */

#ifndef SVS_H
#define SVS_H

struct Svs {
    int count;			/* total number of strings */
    char **vector;		/* vector containing strings */
    size_t path_max;		/* maximum allowable string length */
};

/* Create an empty structure, returns NULL on memory error */
struct Svs *
svs_create(size_t path_max);

/* create a structure from a array of pointers to strings, returns
   NULL on memory error */
struct Svs *
svs_batch_create(char **vector, size_t size, size_t path_max);

/* Free the memory of the entire structure and it's contents  */
int
svs_batch_delete(struct Svs **Svs);

/* Removes the pointer in vector at index, freeing the memory. As a
   side effect the index is reduced by one to reflect the new position
   in the array. Returns 0 on success or 1 after a memory error */
int
svs_line_delete(struct Svs **Svs, int *index);

/* Append a new string to the vector, returns 0 on success or 1
   with memory error */
int
svs_line_append(struct Svs **Svs, const char *new);

/* Append strings to the vector, returns 0 on success or 1
   with memory error */
int
svs_batch_append(struct Svs **Svs, char **vector, size_t length);

/* Print each string pointed to in the vector */
int
svs_batch_print(struct Svs **Svs);

#endif	/* SVS_H */
