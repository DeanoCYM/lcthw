/* SVS.C (string vector structure)
 * --------------------------------------------------
 * Structure containing, and methods for manipulating, an array of
 * pointers to strings
 *
 * (c) Ellis Rhys Thomas <e.rhys.thomas@gmail.com>
 * 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "svs.h"
#include "../ert_log/ert_log.h"

/* --------------------------------------------------
 * Callbacks (test_cb and action_cb)
 * --------------------------------------------------
 * 
 * Callbacks are used internally for looping using
 * svs_loop(). Interface callbacks are in header file.
 * 
 * */

typedef int(*test_cb)(struct Svs **, int *);
typedef int(*action_cb)(struct Svs **, int *);

/* --------------------------------------------------
 * Static Functions
 * --------------------------------------------------
 * 
 * All static functions return 0 on success and 1 on failure, unless
 * otherwise noted. On failure, a message will be logged to stderr
 * along with the errno if appropriate.
 * 
 */

/* Generic loop for callbacks that can perform a binary test and
 * perform the required action if that test returns true.
 *
 * Has side effect of incrementing index, which should be corrected by
 * caller if not required. */
static int
svs_loop(struct Svs **Svs, int *index,
	     test_cb test, action_cb action)
{
    int rc;
    while ( *index < (*Svs)->count ) {
	if (test(Svs, index)) {
	    rc = action(Svs, index);
	    if (rc) {		/* error printed (stderr) in action */
		return rc;
	    }
	}
	++(*index);
    }
    return 0;
}

/* Determine if pointer to string is NULL */
static int
line_null_cb(struct Svs **Svs, int *index)
{
    return ( (*Svs)->vector[*index] == NULL ) ? 1 : 0;
}

static int
line_not_null_cb(struct Svs **Svs, int *index)
{
    return !(line_null_cb(Svs, index));
}

/* Always returns true, useful when no test is required. */
static int
line_no_test_cb(struct Svs **Svs, int *index)
{
    return 1;
}

/* Free a single string in vector */
static int
line_free_cb(struct Svs **Svs, int *index)
{
    free( (*Svs)->vector[*index] );
    (*Svs)->vector[*index] = NULL;
    return 0;
}

/* Overwrite indexed string with subseqent string */
static int
line_shuffleback_cb(struct Svs **Svs, int *index)
{
    (*Svs)->vector[*index] = (*Svs)->vector[(*index) + 1];
     return 0;
}

/* Increase or decrease the size of vector; deletes vector, logs error
   and returns 1 on failure */
static int
vector_resize(struct Svs **Svs, int change)
{
    size_t new_size = (size_t)((*Svs)->count + change) * (sizeof (char *));
    (*Svs)->vector = realloc((*Svs)->vector, new_size);

    if (!(*Svs)->vector) {
	log_err("Failed to resize string vector");
	svs_batch_delete(Svs);
	return 1;
    }
    
    return 0;
}

/* Ensure pointer is to a valid string */
static int
is_valid_string(const char *string, size_t path_max)
{
    if (!string) return 1;
    if (!strnlen(string, path_max)) return 1;
    return 0;
}
/* --------------------------------------------------
 * Interface Functions
 * --------------------------------------------------
 * 
 * All interface functions return 0 on success or 1 on failure, unless
 * otherwise noted. On failure, a log is printed to stderr along with
 * the errno string using ert_log.h.
 *  
 */

/* Initialise and return a new (empty) structure */
struct Svs *
svs_create(size_t path_max)
{
    struct Svs *Svs = malloc(sizeof (struct Svs));
    if (!Svs) {
	log_err("Memory error creating SVS structure");
	return NULL;
    }

    Svs->count = 0;
    Svs->vector = NULL;
    Svs->path_max = path_max;
    
    return Svs; 
}

/* Append a new pointer to the string to the end of the vector */
int
svs_line_append(struct Svs **Svs, const char *new)
{
    int rc;
    rc = is_valid_string(new, (*Svs)->path_max);
    if (rc) return rc;
    rc = vector_resize(Svs, 1);
    if (rc) return rc;

    (*Svs)->vector[(*Svs)->count] = strndup(new, (*Svs)->path_max);
    ++((*Svs)->count);

    return 0;
}

/* Append multiple strings to vector, dynamically increasing the
   memory for the array */
int
svs_batch_append(struct Svs **Svs, char **vector, size_t length)
{
    int rc;
    while ( (*Svs)->count < length ) {
	rc = svs_line_append(Svs, vector[(*Svs)->count]);
	if (rc) return rc;
    }

    return 0;
}
    
/* Create structure from existing vector */
struct Svs *
svs_batch_create(char **vector, size_t size, size_t path_max)
{
    struct Svs *Svs = svs_create(path_max);
    if (!Svs) return NULL;
    
    int rc = svs_batch_append(&Svs, vector, size);
    if (rc) return NULL;

    return Svs;
}

/* Free a single string, reshuffle, and resize vector so that there
 * are no NULL pointers.
 *
 * As svs_loop() increments index, it must be corrected. Index is
 * reduced by one because the next element is now at the deleted
 * line's index. */
int
svs_line_delete(struct Svs **Svs, int *index)
{
    int rc;
    int o_index = *index;	/* store as loop side effect increments */

    rc = line_free_cb(Svs, index);
    if (rc) return rc;

    rc = svs_loop(Svs, index, line_no_test_cb, line_shuffleback_cb);
    if (rc) return rc;

    rc = vector_resize(Svs, -1);
    if (rc) return rc;
	
    --((*Svs)->count);
    *index = o_index - 1;	/* next element is now at original index */
    return 0;
}

/* Free all dynamically allocated memory in the structure */
int
svs_batch_delete(struct Svs **Svs)
{
    int index = 0;		/* deletes from start */
    int rc;

    if (Svs == NULL) return 1;
    if (*Svs == NULL) return 1;

    rc = svs_loop(Svs, &index, line_not_null_cb, line_free_cb);
    free((*Svs)->vector);
    free(*Svs);
    *Svs = NULL;
    
    return rc;
}

/* Print a single string in vector to stdout */
int
svs_line_print(struct Svs **Svs, int *index)
{
    printf("[%d] %s\n", *index, (*Svs)->vector[*index]);
    return 0;
}
   
/* Print each string in vector to stdout */
int 
svs_batch_print(struct Svs **Svs)
{
    log_info("Batch print structure");
    int index = 0;		/* prints from start */
    int rc;
    rc = svs_loop(Svs, &index, line_not_null_cb, svs_line_print);
    if (rc) log_warn("Printing not complete due to previous error");
    return rc;
}
