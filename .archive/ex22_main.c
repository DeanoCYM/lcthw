#include "ex22.h"
#include "dbg.h"

const char *MY_NAME = "Zed A. Shaw";

void scope_demo(int count)
{
    log_info("count is: %d", count);

    if (count > 10) {
	int count = 100;	// BAD! BUGS!

	log_info("count in this scope is %d", count);
    }

    log_info("count is at exit: %d", count);

    count = 3000;

    log_info("count after assign: %d", count);

    scope_demo(count);
}

int main(int argc, char *argv[])
{
    // test out THE_AGE accessors

    /* MY_NAME is a constant variable of type pointer to character in
     * this file.
     *
     * get_age() is a function defined in ex22.c and declared in the
     * header file. It is the interface used to retrive a static
     * variable in ex22.c called THE_AGE. Because it is a static
     * variable, it cannot be accessed directly and requires the
     * function get_age().
     */
    log_info("My name: %s, age: %d", MY_NAME, get_age());

    set_age(100);

    log_info("My age is now %d", get_age());

    // test out THE_SIZE extern

    /* THE_SIZE is a (global) variable in ex22.c, it is declared as
       external in the header file and defined in ex22.c
     *
     * print_size() is a function declared in the header file and
     * defined in ex22.c it accesses the gobal variable THE_SIZE.
     */
    log_info("THE_SIZE is: %d", THE_SIZE);
    print_size();

    THE_SIZE = 9;

    log_info("THE SIZE is now: %d", THE_SIZE);
    print_size();

    // test the ratio function static

    /* update_ratio() is a function declared in the header file and
     * defined in ex22.h. It contains the static variable ratio which
     * is persistant with each function call.
     */
    log_info("Old Ratio at first: %f", update_ratio(2.0));
    log_info("Old Ratio again: %f", update_ratio(10.0));
    log_info("Old Ratio once more: %f", update_ratio(300.0));

    // test the scope demo
    static int count = 4;
    scope_demo(count);
    scope_demo(count * 20);

    log_info("count after calling scope_demo: %d", count);

    log_info("RATIO STOLEN FROM POINTER OUTSIDE SCOPE %f", *RATIO_PTR);

    /* extra credit */

    return 0;
}
