#ifndef logfind_h
#define logfind_h

#define CONFIG_PATH "./.logfind"

/* The mode logfind run in when searching for strings within logfind */
extern enum Mode {AND, OR};

struct PathList *
get_config_PathList(char *config_path, size_t path_max);

struct PathList *
globify_config_PathList(struct PathList **PatternHead, size_t path_max);

#endif	/* logfind_h */
