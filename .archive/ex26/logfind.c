/* logfind.c - A tool to search for strings within log files
*
* Ellis Rhys Thomas <e.rhys.thomas@gmail.com>
* 
* This tool takes any sequence of words and assumes I mean ”and” for
* them. So logfind zedshaw smart guy will find all files that have
* zedshaw and smart and guy in them. It takes an optional argument of
* -o if the parameters are meant to be or logic.
*
* It loads the list of allowed log files from ~/.logfind. The list
* of file names can be anything that the glob function allows. Refer
* to man 3 glob to seehow this works.
*
* */

#include <stdio.h>
#include <stdlib.h>
#include <glob.h>
#include <limits.h>

#include "ert_log/ert_log.h"	/* error logging */

#define DOTFILE "./.logfind"	/* config file w/ paths to logfiles */

/* Holds variables related to a particular file */
struct Fvar {
    char *pathname;
    size_t pathmax;	   /* maximum length of pathname and buffer */
    FILE *stream;
    char **linev;      /* file contents by line */
    size_t linec;      /* line count */
    char *buf;	       /* temporary buffer used when processing */
    size_t buflen;     /* actual length of string in current buffer */
};

/* Holds varaibles related to the search strings */
struct Search {		/* search strings */
    char **needles;
    size_t count;
    size_t maxlen;
    enum search_mode {AND, OR} mode;	/* Search mode */
};

/* Returns true if needle is found in haystack */
int
string_in_line(const char *needle, const char *haystack)
{
    if (needle[0] == '\0') {
	log_warn("Empty string in search parameters");
	return 0;
    }

    if (!strstr(needle, haystack)) return 0;

    return 1;			/* string found */
}    

void search_destroy(struct Search *S)
{
    if (!S) {			/* assert valid structure before deref */
	log_warn("Cannot destroy empty structure");
	return;
    }

    S->count = 0;
    S->maxlen = 0;

    if (S->needles) free(S->needles);
    free(S);

    return;
}

/* Obtain the search strings from user input and determine if the
   program has been run in AND or OR mode. */
struct Search *
search_read_args(size_t argc, char **argv, size_t maxlen)
{
    int i;
    struct Search *S;

    S = malloc(sizeof *S);
    if (!S) {
	log_err("Memory error");
	exit(1);
    }

    S->needles = calloc(argc, sizeof (char *));
    if (!S->needles) {
	log_err("Memory error");
	free(S);
	exit(1);
    }

    S->count = 0;
    S->mode = AND;		/* default to and, set to or if detected */
    S->maxlen = maxlen;

    for (i = 1; i < argc; ++i) {
	if (strncmp("-o", argv[i], S->maxlen) == 0) {	/* OR argument */
	    S->mode = OR;
	} else if (argv[i][0] != '\0') {        /* non empty search string */
	    S->needles[S->count] = argv[i];
	    ++S->count;
	}
    }

    if (S->count == 0) {	/* no search strings provided by user */
	printf("USAGE: %s [-o] string1, string2 ...\n", argv[0]);
	search_destroy(S);
	exit(1);
    }
    
    S->needles = realloc(S->needles, (S->count) * sizeof (char *));
    if (!S->needles) {
	log_err("Memory Error");
	search_destroy(S);
    }

    return S;
}
/* Allocate memory for an Fvar structure */
struct Fvar *
fvar_create(const char *pathname, size_t pathmax)
{
    struct Fvar *F = calloc(1, sizeof (struct Fvar));
    if (!F) {
	log_err("Failed to allocate memory for %s", pathname);
	return NULL;
    }
    log_debug("Creating new struct for %s (pathmax == %zu)",
	      pathname, pathmax);
    
    F->pathname = strndup(pathname, pathmax);
    F->pathmax = pathmax;

    return F;
}

void
fvar_stream_close(struct Fvar *F)
{
    int rc;
    if (!F->stream) {
	log_warn("Cannot close file %s, stream does not exist", F->pathname);
	return;
    }

    rc = fclose(F->stream);
    if (rc) log_warn("Failed to close file %s", F->pathname);
    F->stream = NULL;    

    return;
}

/* Free all dynamically allocated memory, close stream and set all
   values to 0 in in the provided Fvar structure */
void
fvar_destroy(struct Fvar *F)
{
    if (!F) {
	log_warn("Attempted to destroy empty structure");
	return;
    }

    if (F->stream) fvar_stream_close(F);

    if (F->pathname) free(F->pathname);
    if (F->buf) free(F->buf);

    while (F->linec) {
	--F->linec;
	free(F->linev[F->linec]);
    }
    
    F->buflen = 0;
    F->pathmax = 0;
    if (F->linev) free(F->linev);
    free(F);

    return;
}

/* Determine the length of F.buffer and store in F.buflen */
void
fvar_buf_getlen(struct Fvar *F)
{
    F->buflen = strnlen(F->buf, F->pathmax - 1);

    if (F->buflen == 0) {
	log_warn("Empty line in %s", F->pathname);
    } else if (F->buflen == F->pathmax -1) {
	log_warn("String not null terminated! in %s", F->pathname);
	F->buf[F->buflen] = '\0';
    }	

    return;
}    
/* Removes newline characters */
void
fvar_buf_delnl(struct Fvar *F)
{
    fvar_buf_getlen(F);		/* ensure buflen is correct */

    if (F->buflen > 0 && F->buf[F->buflen -1] == '\n')
	F->buf[F->buflen -1] = '\0';

    return;
}

/* Open a file read only and return a stream */
int
fvar_stream_openro(struct Fvar *F)
{
    if (!F->pathname) {
	log_err("Invalid path name");
	goto error;
    }
    
    F->stream = fopen(F->pathname, "r");
    if (!F->stream) {
	log_err("Failed to open %s", F->pathname);
	goto error;
    }

    return 0;
 error:
    	fvar_destroy(F);
	return 1;
}

/* Dynamically allocate memory for a buffer to hold a string. Returns pointer to the buffer */
int
fvar_buf_alloc(struct Fvar *F)
{
    F->buf = calloc(1, F->pathmax);
    if (!F->buf) {
	log_err("Failed to allocate buffer memory for %s", F->pathname);
	fvar_destroy(F);
	return 1;
    }
    return 0;
}

/* Reallocate memory for linev so that it is reflective of linec. If
   linev is reducing, memory should be free'd before use. */
int
fvar_linev_realloc(struct Fvar *F)
{
    F->linev = realloc(F->linev, sizeof (char **) * F->linec);
    if (!F->linev) {
	log_err("Failed to reallocte memory for %s contents", F->pathname);
	fvar_destroy(F);
	return 1;
    }
    return 0;
}

/* Read a line from stream and copy it into buffer */
int
fvar_buf_from_stream(struct Fvar *F)
{
    if (!fgets(F->buf, F->pathmax, F->stream)) {
	log_debug("Reached EOF or error reading file %s)", F->pathname);
	return 1;
    }

    fvar_buf_delnl(F); // BUG: not removing newline!!
    return 0;
}

/* Copy the contents of buffer into linev, reallocating with less
   memory if possible */
int
fvar_linev_from_buf(struct Fvar *F)
{
    int rc;

    if (!F->buf) {
	log_warn("Buffer empty, cannot copy");
	return 1;
    }

    /* pointer array must be increased to accommodate new string */
    ++F->linec;			
    rc = fvar_linev_realloc(F);
    if (rc) return 1;

    F->linev[F->linec - 1] = strndup(F->buf, F->pathmax);
    if (!F->linev[F->linec - 1]) {
	log_err("Memory error copying buffer to vector");
	fvar_destroy(F);
	return 1;
    }

    return 0;
}

/* Replaces patterns in linev with actual paths using glob */
int
fvar_linev_glob(struct Fvar *F)
{
    int i, rc;
    glob_t globbuf;

    for (i = 0; i < F->linec; i++) {
	rc = glob(F->linev[i], (i == 0) ? 0 : GLOB_APPEND, 0, &globbuf);

	switch (rc) {
	case GLOB_NOSPACE:
	    goto fail;
	case GLOB_ABORTED:
	    goto fail;
	case GLOB_NOMATCH:
	    log_info("No files match '%s'", F->linev[i]);
	}
    }
    
    while (F->linec) {		/* free pattern memory */
	--F->linec;
	free(F->linev[F->linec]);
    }
    
    F->linec = globbuf.gl_pathc;
    rc = fvar_linev_realloc(F);
    if(rc) goto fail;
    
    for (i = 0; i < F->linec; i++) {
	F->linev[i] = strndup(globbuf.gl_pathv[i], F->pathmax);
	if (!F->linev[i])  goto fail;
    }
    
    globfree(&globbuf);
    return 0;
 fail:
    log_err("Memory error resolving path patterns");
    globfree(&globbuf);
    return 1;
}

/* Initialises buffer by opening the stream in pathfile element, allocating a buffer the size of pathmax element, and copying each line to the linev vector  */
int
fvar_init(struct Fvar * F)
{
    int rc;

    rc = fvar_stream_openro(F);
    if (rc) return 1;

    rc = fvar_buf_alloc(F);
    if (rc) return 1;

    while (!fvar_buf_from_stream(F)) { /* each line in file */
	rc = fvar_linev_from_buf(F);
	if (rc) return 1;	/* memory error*/
    }
    return 0;
}


/* Opens the configuration file, and appends each line to a Fvar
   structure. Closes the configuration file when complete. */
struct Fvar *
read_dotfile(char *pathname, size_t pathmax)
{
    int rc;
    struct Fvar *DotFile;

    DotFile = fvar_create(pathname, pathmax);
    if (!DotFile) goto fail;
    
    rc = fvar_init(DotFile);
    if (rc) goto fail;

    rc = fvar_linev_glob(DotFile);
    if (rc) goto fail;

    free(DotFile->buf);
    DotFile->buf = NULL;	/* prevents access in fvar_destroy */

    fvar_stream_close(DotFile);

    return DotFile;
 fail:
    fvar_destroy(DotFile);
    return NULL;
}

/* Opens the log file, and appends each line to a Fvar
   structure. Closes the configuration file when complete. */
struct Fvar *
read_logfile(char *pathname, size_t pathmax)
{
    int rc;
    struct Fvar *LogFile;

    LogFile = fvar_create(pathname, pathmax);
    if (!LogFile) goto fail;
    
    rc = fvar_init(LogFile);
    if (rc) goto fail;

    free(LogFile->buf);
    LogFile->buf = NULL;	/* prevents access in fvar_destroy */

    fvar_stream_close(LogFile);

    return LogFile;
 fail:
    fvar_destroy(LogFile);
    return NULL;
}

void
fvar_linev_print(struct Fvar *F)
{
    int i;
    log_info("Printing linev for %s", F->pathname);
    for (i = 0; i < F->linec; i++)
	printf("\t[%d] %s\n", i, F->linev[i]);

    return;
}


/* Create an array of fvar structures of length provided by the DotFile fvar structure linec field. Intended for use converting DotFile into array of LogFiles */
struct Fvar **
fvar_array_alloc(size_t linec)
{
    struct Fvar **LogFiles;
    LogFiles = calloc(linec, sizeof *LogFiles);
    if (!LogFiles) {
	log_err("Memory allocation error");
	return NULL;
    }    

    return LogFiles;
}

void
fvar_array_destroy(struct Fvar **LogFiles, size_t linec)
{
    int i;
    for (i = 0; i < linec; ++i) fvar_destroy(LogFiles[i]);
    free(LogFiles);
    return;
}

struct Fvar **
read_all_logfile(struct Fvar *DotFile)
{
    struct Fvar **LogFile= fvar_array_alloc(DotFile->linec);
    
    for (int i = 0; i < DotFile->linec; i++) { 
	LogFile[i] = read_logfile(DotFile->linev[i], DotFile->pathmax);
	if (!LogFile[i] || LogFile[i]->linec == 0) {
	    log_err("Failed could not read log file");
	    return NULL;
	}
	//fvar_linev_print(LogFile[i]);
    }
    return LogFile;
}

/* Returns 1 if all c elements of idx are non-zero, otherwise returns
   0 or -1 on error. */
int
all_true(int *idx, size_t c)
{
    int i, res;

    if (!idx || c == 0) {
	log_err("Invalid args");
	return -1;
    }
    for (i = 0, res = 0; i < c; ++i)
	if (idx[i]) ++res;

    return (res == c) ? 1 : 0;
}

/* Returns 1 if any c elements of idx are non-zero, otherwise returns
   0 or -1 on error. */
int
any_true(int *idx, size_t c)
{
    int i, res;

    if (!idx || c == 0) {
	log_err("Invalid args");
	return -1;
    }
    for (i = 0, res = 0; i < c; ++i)
	if (idx[i]) ++res;

    return (res >= 1) ? 1 : 0;
}

/* Returns 1 if needle string ss is found in haystack string,
   otherwise 0 or -1 on error */
int
str_in_str(const char *haystack, const char *needle)
{
    if (!needle || !haystack|| !strcmp(needle, "") || !strcmp(haystack, "")) {
	log_err("Invalid string args");
	return -1;
    }

    return strstr(haystack, needle) ? 1 : 0;
}

void
print_logfiles_with_search_strings(struct Fvar **LogFile,
				   struct Search *S,			
				   struct Fvar *DotFile)
{
    struct Fvar *cur_file;
    char *cur_line, *cur_str;
    size_t filec, linec, strc;
    int fi, li, si, **res, i;

    strc = S->count;
    filec = DotFile->linec;
    res = calloc(DotFile->linec, sizeof *res); /* result storage */

    for (fi = 0; fi < filec; fi++) {
	cur_file = LogFile[fi];
	linec = cur_file->linec;
	res[fi] = calloc(strc, sizeof **res);
			
	for (li = 0; li < linec; li++) {
	    cur_line = cur_file->linev[li];

	    for (si = 0; si < strc; si++) {
		cur_str = S->needles[si];

		if (str_in_str(cur_line, cur_str)) {
		    res[fi][si] = 1;
		    log_info("Found %s in %s from %s",
			     cur_str, cur_line, cur_file->pathname);
		} 
	    } 
	}
	switch (S->mode) {	/* print results */
	case AND:
	    if (all_true(res[fi], strc))
		printf("%s\n", cur_file->pathname);
	    break;
	case OR:
	    if (any_true(res[fi], strc))
		printf("%s\n", cur_file->pathname);
	    break;
	}
    }	      

    for (i = 0; i < filec; ++i) free(res[i]);
    free(res);
    return;
}

int
main(int argc, char *argv[])
{
    struct Search *Search;
    struct Fvar *DotFile, **LogFile;
    
    Search = search_read_args(argc, argv, _POSIX_ARG_MAX);
    log_info("Search set to '%s'", (Search->mode == AND) ? "and" : "or");

    /* Configuration file contains log file paths */
    DotFile = read_dotfile(DOTFILE, PATH_MAX);
    if (!DotFile || DotFile->linec == 0) {
	log_err("Failed could not read configuration file");
	return 1;
    }

    /* reading logfiles */
    LogFile = read_all_logfile(DotFile);
    if (!DotFile) {
	log_err("Failed to read logfiles");
	fvar_array_destroy(LogFile, DotFile->linec);
	fvar_destroy(DotFile);
	search_destroy(Search);
	return 1;
    }

    print_logfiles_with_search_strings(LogFile, Search, DotFile);

    fvar_array_destroy(LogFile, DotFile->linec);
    fvar_destroy(DotFile);
    search_destroy(Search);
    
    return 0;
}
