#include <stdio.h>
#include <ctype.h>
#include "dbg.h"

typedef struct lala { int a; int b; } Test;

int print_a_message(const char *msg, size_t len)
{
    printf("A STRING: %s\n", msg);

    return 0;
}


int uppercase(const char *msg, size_t len)
{
    int i = 0;

    // BUG: \0 termination problems
    for (i = 0; msg[i] != '\0' && i < len; i++) {
	printf("%c", toupper(msg[i]));
    }

    printf("\n");

    return 0;
}

int lowercase(const char *msg, size_t len)
{
    int i = 0;

    // BUG: \0 termination problems
    for (i = 0; msg[i] != '\0' && i < len; i++) {
	printf("%c", tolower(msg[i]));
    }

    printf("\n");

    return 0;
}

int fail_on_purpose(const char *msg, size_t len)
{
    return 1;
}

	    
