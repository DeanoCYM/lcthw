#!/bin/bash

set -e

./ex17 df c 5 50
./ex17 df l

./ex17 df s 0 rhys r@thomas.com "Chemical Engineer" 
./ex17 df s 2 ellis e@thomas.com "Dude"
./ex17 df s 4 thomas t@thomas.com "Degenerate"

echo "---"

./ex17 df g 4

echo "---"

./ex17 df d 4
./ex17 df l

echo "---"

./ex17 df f ellis name
./ex17 df f Dude job
