#include "dbg.h"
#include <stdio.h>
#include <stdlib.h>

void test_debug()
{
    // notice you dont need the \n
    debug("I have brown hair.");

    // passing in arguments like printf
    debug("I am %d years old.", 37);
}

void test_log_err()
{
    log_err("I believe everything is broken.");
    log_err("There are %d problems in %s.", 0, "space");
}

int main(int argc, char *argv[])
{
    check(argc == 2, "Need an arguement");

    test_debug();
    test_log_err();



    return 0;

 error:
    return 1;
}
