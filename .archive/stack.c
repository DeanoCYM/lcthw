#include <stdio.h>
#include <stdlib.h>

struct Element {
    int value;
    struct Element *next;
};

struct Element *
push(int value, struct Element *cur_top)
{
    struct Element *new_top = malloc(sizeof (struct Element));
    new_top->value = value;
    new_top->next = cur_top;

    printf("Pushed %d to stack...\n", value);

    return new_top;
}

struct Element *
pop(struct Element *cur_top)
{
    if (cur_top == NULL) {
	printf("WARNING: Stack empty.\n");
	return cur_top;
    }

    struct Element *new_top = cur_top->next;
    printf("Pop'd %d from stack.\n", cur_top->value);
    free(cur_top);

    return new_top;
}

int main(int argc, char *argv[])
{
    struct Element *stack = push(100, NULL);
    
    // stack: [ 100 ]
    stack = pop(stack);            // stack: [ ] -> 100
    stack = pop(stack);            // stack: [ ] -> empty error
    stack = push(200, stack);      // stack: [ 200 ]
    stack = push(50, stack);	   // stack: [ 200 50 ]
    stack = push(30, stack);	   // stack: [ 200 50 30 ]
    stack = pop(stack);            // stack: [ 200 50 ] -> 30
    stack = push(20, stack);       // stack: [ 200 50 20]
    stack = pop(stack);            // stack: [ 200 50] -> 20
    stack = pop(stack);            // stack: [ 200 ] -> 200
    stack = pop(stack);            // stack: [ ]
    stack = pop(stack);            // stack: [ ] -> empty error
    
    return 0;
}
