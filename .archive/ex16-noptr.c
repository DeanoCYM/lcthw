#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

// prototype for a structure called Person 
struct Person {
    char *name;			/* declare name field of type pointer to char */
    int age;			/* declare age field of type int */
    int height;			/* declare height field of type int */
    int weight;			/* declare weight field of type int */
};

// function called person that returns a Person structure
struct Person Person_create(char *name, int age, int height, int weight)
{
    struct Person who;

    who.name = name;
    who.age = age;	     
    who.height = height;	
    who.weight = weight;

    return who;
}

// print each field in the Person structure
void Person_print(struct Person who)
{
    printf("Name: %s\n", who.name);
    printf("\tAge: %d\n", who.age);
    printf("\tHeight: %d\n", who.height);
    printf("\tWeight: %d\n", who.weight);
}

int main(int argc, char *argv[])
{
    // make two people structures
    struct Person joe = Person_create("Joe Alex", 18, 137, 100);
    struct Person rhys = Person_create("Rhys Thomas", 28, 185, 100);

    // print them out where they are in memory
    printf("Joe is at memory location %p.\n", &joe);
    Person_print(joe);

    printf("Rhys is at memory location %p.\n", &rhys);
    Person_print(rhys);
    
    //make everyone age and print them again
    joe.age += 20;
    joe.height -= 2;
    joe.weight +=10;
    Person_print(joe);

    rhys.age += 20;
    rhys.height -=1;
    
    rhys.weight -= 20;
    Person_print(rhys);

    return 0;
}
